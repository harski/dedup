# Dedup

Dedup is a program for finding duplicate files in the file system.

Two files in the file system are considered to be duplicates of each other if
their sha1 checksums are equal.


## Usage

Basec usage is easy:

```sh
# simple invocation for current directory
dedup .

# invocation for any directory
dedup input-dir
dedup /path/to/dir

# supports multiple arguments
dedup input-dir-1 input-dir-2 input-dir-3
```

By default `dedup` prints only the duplicates it found i.e. it is safe to
remove all the files in the output without losing any information. In verbose
mode (`--verbose` or `-v`) all of the file paths for a certain file are
printed.


## Dependencies

Besides the packages mentioned in `Cargo.toml`, the package needs `sqlite` to
be installed on the system. On `Debian Buster` the needed packages are
`sqlite3` and `libsqlite3-dev`:

```sh
# Debian Buster
apt install sqlite3 libsqlite3-dev
```


## License

This project is licenced under the 2-clause BSD license. See the LICENSE file
for details.
