extern crate clap;
extern crate dedup;

use clap::{App, Arg};
use dedup::cache::{Cache, CacheFile, SqliteCache, DEFAULT_CACHE_PATH};
use dedup::fileinfo::{FileInfo, FileInfos};
use dedup::input_queue::InputQueue;
use std::collections::HashMap;
use std::path::PathBuf;

const BIN_NAME: &str = env!("CARGO_PKG_NAME");
const VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Debug)]
struct Settings {
    pub cache_file: PathBuf,
    pub verbose: bool,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            cache_file: PathBuf::from(DEFAULT_CACHE_PATH),
            verbose: false,
        }
    }
}

#[derive(Debug)]
struct Stats {
    cache_hits: u64,
    file_cnt: u64,
}

impl Default for Stats {
    fn default() -> Self {
        Stats {
            cache_hits: 0,
            file_cnt: 0,
        }
    }
}

pub fn main() {
    let mut settings = Settings::default();
    let mut stats = Stats::default();

    let args = App::new(BIN_NAME)
        .version(VERSION)
        .about("Find duplicate files.")
        .arg(
            Arg::with_name("INPUT-DIR")
                .help("Set the input directory")
                .multiple(true)
                .required(true),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .long("verbose")
                .help("Verbose output.")
                .takes_value(false)
                .required(false),
        )
        .get_matches();

    if args.is_present("verbose") {
        settings.verbose = true;
    }

    // Input file queue.
    let mut files_in: InputQueue = InputQueue::default();

    // Insert dirs and files from CLI to the input queue.
    for path in args.values_of("INPUT-DIR").unwrap() {
        let path = PathBuf::from(path);
        files_in.push_back(path).unwrap();
    }

    // Walk the input directories and files, find duplicates and then print them.
    let files = walk_files(files_in, &settings, &mut stats);
    print_duplicate_files(HashMap::from(files), settings.verbose);
}

/// Print file paths of the found duplicates.
///
/// Prints the duplicate files, one path per line. If `verbose` is not set, it is safe to delete
/// all the file paths mentioned in the output without losing any information.
///
/// If `verbose` is set, the file sha1 hash is printed on the first line, and then intended by a
/// '\t' all of the file copies (including the "original") on the subsequent lines.
fn print_duplicate_files(map: HashMap<String, FileInfo>, verbose: bool) {
    let print_fn = if verbose {
        print_duplicate_verbose
    } else {
        print_duplicate
    };

    for (_, fi) in map {
        if fi.paths.len() > 1 {
            print_fn(&fi);
        }
    }
}

fn print_duplicate(fi: &FileInfo) {
    for path in fi.paths.iter().skip(1) {
        println!("{}", path.to_str().unwrap());
    }
}

fn print_duplicate_verbose(fi: &FileInfo) {
    println!("{}", fi.digest);
    for path in &fi.paths {
        println!("\t{}", path.to_str().unwrap());
    }
}

/// Traverse the input dirctories/files and find the duplicates.
///
/// Traverses the `files_in` `InputQueue` recursively, adding all the found files to the
/// `FileInfos` structure.
///
/// The function use `SqliteCache` as a cache backend. All found files are looked up from and/or
/// inserted in to the cache, saving reading the files and calculating the file sha1sums on the
/// subsequent runs of the program.
fn walk_files(mut files_in: InputQueue, settings: &Settings, stats: &mut Stats) -> FileInfos {
    let mut files = FileInfos::default();
    let mut cache = SqliteCache::new(&settings.cache_file).unwrap();

    // TODO: fix the unwraps
    while let Some(file) = files_in.pop_front().unwrap() {
        stats.file_cnt += 1;

        if settings.verbose {
            println!(
                "handling target {} - {}",
                stats.file_cnt,
                file.to_str().unwrap()
            );
        }

        // fetch through cache and insert
        let cache_file = match cache.get(&file).unwrap() {
            Some(cache_file) => {
                stats.cache_hits += 1;
                cache_file
            }
            None => {
                let cache_file = CacheFile::new(&file).unwrap();
                if let Err(e) = cache.insert(&cache_file) {
                    eprintln!("{}", e);
                };
                cache_file
            }
        };
        let _ = files.insert(cache_file);
    }

    files
}
