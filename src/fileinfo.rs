//! Resources for handled, hashed file entries.

use crate::cache::CacheFile;
use sha1::{Digest, Sha1};
use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Read};
use std::path::PathBuf;

/// The main file struct.
pub struct FileInfo {
    pub digest: Digest,
    pub paths: Vec<PathBuf>,
}

impl FileInfo {
    /// Initialize a new FileInfo.
    ///
    /// Calculate the digest and add the path to the path list.
    ///
    /// ```
    /// use dedup::fileinfo::FileInfo;
    /// use std::path::PathBuf;
    ///
    /// let fi = FileInfo::new(PathBuf::from("Cargo.toml"));
    /// assert!(fi.is_ok());
    /// ```
    pub fn new(path: PathBuf) -> io::Result<FileInfo> {
        let digest = get_sha1sum(&mut File::open(&path)?)?;
        let mut paths = vec![];
        paths.push(path);
        Ok(FileInfo { digest, paths })
    }
}

impl fmt::Display for FileInfo {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.digest.to_string())
    }
}

impl From<Digest> for FileInfo {
    fn from(digest: Digest) -> FileInfo {
        let paths = Vec::new();
        FileInfo { digest, paths }
    }
}

/// An abstracted collection for holding FileInfo structs.
pub struct FileInfos {
    infos: HashMap<String, FileInfo>,
}

impl Default for FileInfos {
    /// Get default, empty FileInfos structure.
    fn default() -> FileInfos {
        FileInfos {
            infos: HashMap::new(),
        }
    }
}

impl FileInfos {
    /// Insert a CacheFile in to the backend.
    ///
    /// ```
    /// use dedup::cache::CacheFile;
    /// use dedup::fileinfo::FileInfos;
    /// use std::path::PathBuf;
    ///
    /// let mut fileinfos = FileInfos::default();
    /// let file = CacheFile::new(&PathBuf::from("Cargo.toml")).unwrap();
    /// assert_eq!((), fileinfos.insert(file).unwrap());
    /// ```
    pub fn insert(&mut self, file: CacheFile) -> io::Result<()> {
        let entry = self
            .infos
            .entry(file.digest.to_string())
            .or_insert_with(|| FileInfo::from(file.digest));
        entry.paths.push(file.path);
        Ok(())
    }
}

impl From<FileInfos> for HashMap<String, FileInfo> {
    fn from(fi: FileInfos) -> HashMap<String, FileInfo> {
        fi.infos
    }
}

/// Calculate sha1sum for `Read`.
///
/// ```
/// use dedup::fileinfo::get_sha1sum;
/// use std::fs::File;
/// use std::path::PathBuf;
///
/// let path = PathBuf::from("Cargo.toml");
/// let mut file = File::open(&path).unwrap();
/// assert!(get_sha1sum(&mut file).is_ok());
/// ```
pub fn get_sha1sum(source: &mut dyn Read) -> io::Result<Digest> {
    let mut sha1 = Sha1::new();
    let mut reader = BufReader::new(source);

    loop {
        let buffer = reader.fill_buf()?;
        let len = buffer.len();

        if len == 0 {
            break;
        }

        sha1.update(&buffer);
        reader.consume(len);
    }

    Ok(sha1.digest())
}
