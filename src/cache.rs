//! Cache functionality for `FileInfo`s.
//!
//! If it is likely that the same files will be traversed another time (for example periodically or
//! if the process fails or is otherwise necessary to restart) it is recommended to put all the
//! calculated hash to the cache. It will speed up reprocessing significantly.

use crate::error::DedupError;
use crate::fileinfo::get_sha1sum;
use rusqlite::{self, params, Connection, Rows, NO_PARAMS};
use sha1::Digest;
use std::fs::File;
use std::io;
use std::path::{Path, PathBuf};
use std::result::Result;
use std::str::FromStr;

pub const DEFAULT_CACHE_PATH: &str = ".cache";

/// Cache entry.
///
/// Contains the data that should be inserted in to the cache backend.
pub struct CacheFile {
    pub digest: Digest,
    pub path: PathBuf,
    pub size: u64,
}

/// Sqlite cache backend.
///
/// Handles writing new cache entries to the disk and loading then back on re-initialization.
pub struct SqliteCache {
    conn: Connection,
}

/// The interface for a cache implementation.
///
/// Some of the provided functions, especially `insert_all()` may be naive and inefficient. When
/// implementing this trait, look carefully if the default functions can be improved upon in the
/// case of your backend.
pub trait Cache {
    /// Clear (delete) the cache.
    fn clear(&mut self) -> Result<(), DedupError>;

    /// Get the CacheFile if it exists.
    fn get(&self, path: &Path) -> Result<Option<CacheFile>, DedupError>;

    /// Get all cached files.
    fn get_all(&self) -> Result<Vec<CacheFile>, DedupError>;

    /// Get file from cache or insert it if it doesn't exist.
    fn get_or_insert(&mut self, path: &Path) -> Result<CacheFile, DedupError> {
        match self.get(path)? {
            Some(file) => Ok(file),
            None => Ok(CacheFile::new(path)?),
        }
    }

    /// Insert a file in to the cache.
    fn insert(&mut self, file: &CacheFile) -> Result<(), DedupError>;

    /// Mass-insert files.
    fn insert_all(&mut self, files: &[CacheFile]) -> Result<(), DedupError> {
        for file in files {
            self.insert(file)?;
        }
        Ok(())
    }

    /// Remove a file from the cache.
    fn remove(&mut self, file: &CacheFile) -> Result<(), DedupError>;

    /// Replace the whole cache.
    fn replace(&mut self, files: &[CacheFile]) -> Result<(), DedupError> {
        self.clear()?;
        self.insert_all(files)?;
        Ok(())
    }

    /// Validate the cache and remove stale entries.
    fn validate(&mut self) -> Result<(), DedupError> {
        let files = self.get_all()?;
        for file in files {
            if !file.validate()? {
                self.remove(&file)?;
            }
        }
        Ok(())
    }
}

impl SqliteCache {
    /// Initialize a new sqlite cache database from path.
    ///
    /// If the path exists, the database is loaded and the cache validated. If the path does not
    /// exits a new database is created.
    ///
    /// ```no_run
    /// use dedup::cache::SqliteCache;
    /// use std::path::PathBuf;
    ///
    /// let path = PathBuf::from(".cache");
    /// let cache = SqliteCache::new(&path).unwrap();
    /// ```
    pub fn new(path: &Path) -> Result<SqliteCache, DedupError> {
        let conn = Connection::open(path)?;

        let _ = conn.execute(
            "CREATE TABLE IF NOT EXISTS files (
                path TEXT PRIMARY KEY,
                digest TEXT NOT NULL,
                size INTEGER NOT NULL
            );",
            params![],
        )?;

        let mut cache = SqliteCache { conn };
        cache.validate()?;
        Ok(cache)
    }
}

impl Cache for SqliteCache {
    /// Drop all files from cache.
    fn clear(&mut self) -> Result<(), DedupError> {
        let mut drop_stmt = self.conn.prepare("DELETE FROM files")?;
        let _ = drop_stmt.query(NO_PARAMS)?;
        Ok(())
    }

    /// Get a file from cache.
    ///
    /// If the path was in cache, returns `Ok(Some(CacheFile))`. In case of a cache miss, returns
    /// `Ok(None)`.
    fn get(&self, path: &Path) -> Result<Option<CacheFile>, DedupError> {
        let mut stmt = self
            .conn
            .prepare("SELECT digest, size FROM files WHERE path=?")?;
        // TODO: fix the unwrap
        let res: (String, i64) = match stmt.query_row(params![path.to_str().unwrap()], |row| {
            Ok((row.get(0)?, row.get(1)?))
        }) {
            Ok(t) => t,
            Err(e) => {
                // no results or an error happened
                match e {
                    rusqlite::Error::QueryReturnedNoRows => return Ok(None),
                    _ => return Err(e.into()),
                };
            }
        };

        Ok(Some(CacheFile {
            digest: Digest::from_str(&res.0)?,
            path: PathBuf::from(path),
            size: res.1 as u64,
        }))
    }

    /// Get all files in the cache.
    fn get_all(&self) -> Result<Vec<CacheFile>, DedupError> {
        let mut v = Vec::new();

        let mut stmt = self.conn.prepare("SELECT digest, path, size FROM files")?;
        let mut rows: Rows = stmt.query(NO_PARAMS)?;

        while let Some(row) = rows.next()? {
            let file = CacheFile {
                digest: Digest::from_str(&row.get::<usize, String>(0)?)?,
                path: PathBuf::from(row.get::<usize, String>(1)?),
                size: row.get::<usize, i64>(2)? as u64,
            };

            v.push(file);
        }

        Ok(v)
    }

    /// Insert a file in to the cache.
    fn insert(&mut self, file: &CacheFile) -> Result<(), DedupError> {
        let mut stmt = self
            .conn
            .prepare("INSERT INTO files (digest, path, size) VALUES (?, ?, ?)")?;
        // TODO: fix unwrap()
        let _ = stmt.execute(&[
            file.digest.to_string(),
            file.path.to_str().unwrap().to_string(),
            file.size.to_string(),
        ])?;
        Ok(())
    }

    /// Remove a file from the cache.
    fn remove(&mut self, file: &CacheFile) -> Result<(), DedupError> {
        let mut stmt = self.conn.prepare("DELETE FROM files WHERE path=?")?;
        // TODO: fix unwrap()
        let _ = stmt.query(&[file.path.to_str().unwrap().to_string()])?;
        Ok(())
    }
}

impl CacheFile {
    /// Initialize a new cache file from a file path.
    ///
    /// ```
    /// use dedup::cache::CacheFile;
    /// use std::path::PathBuf;
    /// let path = PathBuf::from("Cargo.toml");
    /// let cache_file = CacheFile::new(&path).unwrap();
    /// ```
    pub fn new(path: &Path) -> Result<CacheFile, DedupError> {
        Ok(CacheFile {
            digest: get_sha1sum(&mut File::open(path)?)?,
            path: PathBuf::from(&path),
            size: path.metadata()?.len(),
        })
    }

    /// Validate that the cached file has not changed.
    ///
    /// Returns the result `true` if the cach file is current, and `false` if the cache entry is
    /// stale.
    ///
    /// ```
    /// use dedup::cache::CacheFile;
    /// use std::path::PathBuf;
    /// let path = PathBuf::from("Cargo.toml");
    /// let cache_file = CacheFile::new(&path).unwrap();
    /// assert!(cache_file.validate().unwrap());
    /// ```
    pub fn validate(&self) -> io::Result<bool> {
        let res = (self.path.exists()) && (self.path.metadata()?.len() == self.size);
        Ok(res)
    }
}
