//! Input file queue.
//!
//!  Walks the directories in a breath first manner.

use crate::error::DedupError;
use std::collections::VecDeque;
use std::fs::read_dir;
use std::path::PathBuf;
use std::result::Result;

/// Backend for the input queue.
pub struct InputQueue {
    dirs: VecDeque<PathBuf>,
    files: VecDeque<PathBuf>,
    others: VecDeque<PathBuf>,
}

impl Default for InputQueue {
    /// Initialize a new empty input file queue.
    ///
    /// ```
    /// use dedup::input_queue::InputQueue;
    /// let mut queue = InputQueue::default();
    /// assert_eq!(None, queue.pop_front().unwrap());
    /// ```
    fn default() -> InputQueue {
        InputQueue {
            dirs: VecDeque::new(),
            files: VecDeque::new(),
            others: VecDeque::new(),
        }
    }
}

impl InputQueue {
    /// Pop a PathBuf from the queue.
    ///
    /// If the input file queue is empty, a next directory in the directory input queue is
    /// unpacked. As long as there are files in the queues, a Result of `Ok(PathBuf)` is returned.
    /// When the queue runs out of files and directories, a result `Ok(None)` is returned.
    ///
    /// ```
    /// use dedup::input_queue::InputQueue;
    /// let mut queue = InputQueue::default();
    /// assert_eq!(None, queue.pop_front().unwrap());
    /// ```
    pub fn pop_front(&mut self) -> Result<Option<PathBuf>, DedupError> {
        while self.files.is_empty() {
            if !self.unpack_next_dir()? {
                break;
            }
        }

        Ok(self.files.pop_front())
    }

    /// Add a file to the input queue.
    ///
    /// ```
    /// use dedup::input_queue::InputQueue;
    /// use std::path::PathBuf;
    ///
    /// let mut queue = InputQueue::default();
    /// queue.push_back(PathBuf::from("Cargo.toml")).unwrap();
    /// assert!(queue.pop_front().unwrap().is_some());
    /// ```
    pub fn push_back(&mut self, path: PathBuf) -> Result<(), DedupError> {
        let md = path.metadata()?;
        if md.is_file() {
            self.files.push_back(path);
        } else if md.is_dir() {
            self.dirs.push_back(path);
        } else {
            self.others.push_back(path);
        }

        Ok(())
    }

    /// Unpack the directory in the directory queue and push the files (dirs) to queue.
    fn unpack_next_dir(&mut self) -> Result<bool, DedupError> {
        match self.dirs.pop_front() {
            Some(dir) => {
                for entry in read_dir(dir)? {
                    let path = entry?.path();
                    self.push_back(path)?;
                }
                Ok(true)
            }
            None => Ok(false),
        }
    }
}
