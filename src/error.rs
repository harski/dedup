//! Dedup library error definitions.

use std::error::Error;
use std::fmt;
use std::io;

/// Error enum for all dedup library errors.
#[derive(Debug)]
pub enum DedupError {
    Io(io::Error),
    Sha1(sha1::DigestParseError),
    Sqlite(rusqlite::Error),
}

impl Error for DedupError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match *self {
            DedupError::Io(ref e) => Some(e),
            DedupError::Sha1(_) => None,
            DedupError::Sqlite(ref e) => Some(e),
        }
    }
}

impl fmt::Display for DedupError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DedupError::Io(ref e) => e.fmt(f),
            DedupError::Sha1(ref e) => e.fmt(f),
            DedupError::Sqlite(ref e) => e.fmt(f),
        }
    }
}

impl From<io::Error> for DedupError {
    fn from(err: io::Error) -> DedupError {
        DedupError::Io(err)
    }
}

impl From<sha1::DigestParseError> for DedupError {
    fn from(err: sha1::DigestParseError) -> DedupError {
        DedupError::Sha1(err)
    }
}

impl From<rusqlite::Error> for DedupError {
    fn from(err: rusqlite::Error) -> DedupError {
        DedupError::Sqlite(err)
    }
}
